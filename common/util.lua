local Vec = require 'common.vec'
local Util = {}

function Util.deepcopy(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
    copy = {}
    for orig_key, orig_value in next, orig, nil do
        copy[Util.deepcopy(orig_key)] = Util.deepcopy(orig_value)
    end
    setmetatable(copy, Util.deepcopy(getmetatable(orig)))
  else
    copy = orig
  end
  return copy
end

function Util.split(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={}
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    table.insert(t, str)
  end
  return t
end

function Util._is_a_valid_position(list, cursor)
  local cursorX, cursorY = cursor:get_position()
  local cell_size = 36

  for _,pos in ipairs(list) do
    -- round position because its square is smaller than the cursor square
    local rounded_position = pos + Vec(5, 5)
    local x, y = rounded_position:get()

    if x >= cursorX and x <= cursorX + cell_size and
      y >= cursorY and y <= cursorY + cell_size then
      return true
    end
  end

  return false
end

function Util.is_in_range(a, b, range)
  local a_x, a_y = a:get()
  local b_x, b_y = b:get()
  return a_x >= b_x - range and a_x <= b_x + range and a_y >= b_y - range and a_y <= b_y + range
end


function Util.concat(t1,t2)
  local t3 = {}
  for i=1, #t1 do
    t3[i] = t1[i]
  end
  for i=1,#t2 do
    t3[#t3+1] = t2[i]
  end
  return t3
end

return Util
