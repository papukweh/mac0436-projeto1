
local Entity = require 'common.class' ()

function Entity:_init(type, specname)
  local path = type and
    'database.entities.' .. type .. '.' .. specname or
    'database.entities.' .. specname
  local spec = require(path)

  self.spec = spec
  self.db_name = specname
  self.hp = spec.max_hp
  self.alive = true
end

function Entity:get_name()
  return self.spec.name
end

function Entity:get_db_name()
  return self.db_name
end

function Entity:get_appearance()
  return self.spec.appearance
end

function Entity:get_hp()
  return self.hp, self.spec.max_hp
end

function Entity:is_alive()
	return self.alive
end

function Entity:get_range()
  return self.spec.range
end


function Entity:get_damage()
  return self.spec.damage
end

function Entity:get_value()
  return self.spec.value
end

function Entity:take_damage(dmg)
	self.hp = self.hp - dmg
	if self.hp <= 0 then
		self.alive = false
	end
end

function Entity:update_component(name, multiplier)
  if self[name] then self[name] = self[name]*multiplier
  else self.spec[name] = self.spec[name]*multiplier end
end

return Entity