local Tactic = require 'common.class' ()

function Tactic:_init(specname)
    local spec = require('database.tactics.' .. specname)
    self.name = spec.name
    self.cost = spec.cost
    self.effect = spec.effect
    self.range = spec.range
    self.color = spec.color
    self.message_success = spec.message_success
    self.message_failure = spec.message_failure
end

function Tactic:get_name()
    return self.name
end

function Tactic:get_cost()
    return self.cost
end

function Tactic:get_effect()
    return self.effect
end

function Tactic:get_message(bool)
    if bool then
        return self.message_success
    end
    return self.message_failure
end

function Tactic:get_range()
    return self.range
end

function Tactic:get_color()
    return self.color
end

return Tactic