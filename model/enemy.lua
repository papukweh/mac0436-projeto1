
local Entity = require 'model.entity'
local Enemy = require 'common.class' (Entity)

function Enemy:_init(specname)
  local spec = require('database.entities.enemies.' .. specname)
  self:super('enemies', specname)
  self.spec = spec
  self.moving = true
end

function Enemy:get_speed()
  return self.spec.speed
end

function Enemy:get_target()
  return self.spec.target
end

function Enemy:is_moving()
  return self.moving
end

function Enemy:halt()
  self.moving = false
end


return Enemy