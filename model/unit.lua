
local Entity = require 'model.entity'
local Unit = require 'common.class' (Entity)

function Unit:_init(specname)
  local spec = require('database.entities.units.' .. specname)
  self:super('units', specname)
  self.spec = spec
end

function Unit:get_cost()
  return self.spec.cost
end

function Unit:get_value()
  return self.spec.value
end

function Unit:get_evolution()
  return self.spec.evolution
end

function Unit:evolves_from()
  return self.spec.evolves_from
end

function Unit:get_stats()
	return self.spec.name .. ": HP:" .. self.spec.max_hp .. " Power:" .. self.spec.damage ..
  " Range:" .. self.spec.range .. " Value:" .. self.spec.value .. " Cost:" .. self.spec.cost
end

function Unit:get_requirement()
  return self.spec.requires
end

function Unit:get_max_per_unit()
  return self.spec.max_per_unit
end

function Unit:get_bonus()
  return self.spec.bonus
end

function Unit:get_limited_position()
  return self.spec.limited_position
end

function Unit:get_support()
  return self.spec.support
end

return Unit
