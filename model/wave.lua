
local Wave = require 'common.class' ()

function Wave:_init(spawns)
  self.spawns = spawns
  self.delay = 3
  self.left = nil
  self.pending = 0
  self.ended = false
  self.started = false
end

function Wave:start(wait)
  self.left = wait
end

function Wave:has_ended()
  return self.ended
end

function Wave:has_started()
  return self.started
end

function Wave:update(dt)
  if not self.ended then
    self.left = self.left - dt
    if self.left <= 0 then
      self.started = true
      self.left = self.left + self.delay
      self.pending = self.pending + 1
    end
  end
  return self.left
end

function Wave:poll()
  local pending = self.pending
  self.pending = 0
  return pending
end

function Wave:spawn()
  for monster, num in pairs(self.spawns) do
    if num > 0 then
      self.spawns[monster] = num - 1
      return monster
    end
  end
  self.pending = 0
  self.ended = true
  self.started = false
  return 'end'
end

return Wave

