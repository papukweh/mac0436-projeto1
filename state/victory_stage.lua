
local PALETTE_DB = require 'database.palette'
local State = require 'state'
local Text = require 'view.text'
local Sound = require 'view.audio'

local VictoryState = require 'common.class' (State)

function VictoryState:_init(stack)
  self:super(stack)
  local h = love.graphics.getHeight()
  local w = love.graphics.getWidth()
  self.atlas = nil
  self.text = Text('', (w/2)-100, (h/2)-100, 100, 100, {1,1,1}, 42)
  self.audio = Sound()
end

function VictoryState:enter(params)
  if params.victory then
    self.text:set_text("You win!")
    self.audio:playMusic('victory_music')
  else
    self.audio:playMusic('game_over')
    self.text:set_text("You lose!")
  end
  love.graphics.setBackgroundColor(PALETTE_DB.black)
  self:view('hud'):add('victory_text', self.text)
end

function VictoryState:leave()
  self.audio:stop()
  self:view('hud'):remove('victory_text')
end

function VictoryState:on_mousepressed(_, _, button)
  if button == 1 then
    local params = {final=true}
    return self:pop(params)
  end
end

function VictoryState:on_keypressed(key)
  if key == 'space' or key == 'return' then
    local params = {final=true}
    return self:pop(params)
  end
end

return VictoryState
