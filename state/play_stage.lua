
local Wave = require 'model.wave'
local Unit = require 'model.unit'
local Entity = require 'model.entity'
local Enemy = require 'model.enemy'
local Tactic = require 'model.tactic'
local Vec = require 'common.vec'
local Cursor = require 'view.cursor'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local Stats = require 'view.stats'
local State = require 'state'
local ListMenu = require 'view.list_menu'
local NumMenu = require 'view.num_menu'
local Util = require 'common.util'
local Text = require 'view.text'
local Position = require 'view.position'
local Range = require 'view.range'
local Sound = require 'view.audio'

local PlayStageState = require 'common.class' (State)

-- Initializes state
function PlayStageState:_init(stack)
  self:super(stack)
  self:init_params()
end

function PlayStageState:init_params()
  self.stage = nil
  self.cursor = nil
  self.atlas = nil
  self.battlefield = nil
  self.available_units = {}
  self.unavailable_units = {}
  self.available_positions = nil
  self.available_tactics = nil
  self.tactics_positions = nil
  self.tactics = {}
  self.units = {}
  self.unit_pos = {}
  self.wave = nil
  self.stats = nil
  self.monsters = {}
  self.game_over = false
  self.current_wave = 1
  self.gild = 0
  self.unit_menu = nil
  self.tactic_menu = nil
  self.capital = nil
  self.log = nil
  self.info = nil
  self.delay = 2
  self.announce = 0
  self.support_units = {}
  self.support_range = nil
  self.current_tactic = nil
  self.audio = Sound()
end

-- Functions for managing the stack
function PlayStageState:enter(params)
  self:init_params()
  self.stage = params.stage
  self.gild = params.stage.gild
  self:_load_view()
  self:_load_units()
  self:_load_menus()
  self:_load_log()
  self:_load_info()
  self.audio:playMusic('battle_music')
end

function PlayStageState:leave()
  self:view('bg'):remove('battlefield')
  self:view('fg'):remove('atlas')
  self:view('bg'):remove('cursor')
  self:view('bg'):remove('positions')
  self:view('bg'):remove('range')
  self:view('bg'):remove('tactics_positions')
  self:view('hud'):remove('stats')
  self:view('hud'):remove('unit_menu')
  self:view('hud'):remove('tactic_menu')
  self:view('hud'):remove('log')
  self:view('hud'):remove('info')
  self.audio:stop()
end

function PlayStageState:suspend()
  self.audio:stop()
  self:view('bg'):remove('battlefield')
  self:view('fg'):remove('atlas')
  self:view('bg'):remove('cursor')
  self:view('bg'):remove('positions')
  self:view('bg'):remove('range')
  self:view('bg'):remove('tactics_positions')
  self:view('hud'):remove('stats')
  self:view('hud'):remove('unit_menu')
  self:view('hud'):remove('tactic_menu')
  self:view('hud'):remove('log')
  self:view('hud'):remove('info')
end

function PlayStageState:resume(params)
  self.audio:playMusic('battle_music')
  if params.final then
    return self:pop()
  end
end

-- Functions for loading different components on the PlayStage
function PlayStageState:_load_view()
  self.battlefield = BattleField()
  self.atlas = SpriteAtlas()
  self.cursor = Cursor(self.battlefield)
  self.available_positions = Position(self.battlefield, 'green')
  self.tactics_positions = Position(self.battlefield, 'red')
  local _, right, top, _ = self.battlefield.bounds:get()
  self.stats = Stats(Vec(right + 16, top), self.gild)
  self:view('bg'):add('battlefield', self.battlefield)
  self:view('fg'):add('atlas', self.atlas)
  self:view('bg'):add('cursor', self.cursor)
  self:view('bg'):add('positions', self.available_positions)
  self:view('bg'):add('tactics_positions', self.tactics_positions)
  self:view('hud'):add('stats', self.stats)
end

-- Wave messages
function PlayStageState:_load_log()
  local w = love.graphics.getWidth()
  self.log = Text("10 segundos até a próxima wave", (w/2)-270, 0, 100, 100, {1,1,1}, 42)
  self:view('hud'):add('log', self.log)
end

-- Units stats
function PlayStageState:_load_info()
  local h = love.graphics.getHeight()
  self.info = Text("Inicializando stats...", 25, h-60, 100, 100, {1,1,1}, 36)
  self:view('hud'):add('info', self.info)
  self.info:set_text(Unit(self.available_units[1].name):get_stats())
end

function PlayStageState:_load_menus()
  local tactics = love.filesystem.getDirectoryItems('database/tactics')

  local tactic_params = {}
  for i,v in ipairs(tactics) do
    tactic_params[i] = Util.split(v, '.')[1]
    local tactic = Tactic(tactic_params[i])
    tactic_params[i] = i .. '> ' .. tactic:get_name() .. ' ' .. tactic:get_cost()
    self.tactics[i] = tactic
  end

  -- Loads unit menu
  local h = love.graphics.getHeight()
  self:_get_available_units()
  self.unit_menu = ListMenu(self.available_units)
  self.unit_menu.position = Vec(600, h/2+100)
  self:view('hud'):add('unit_menu', self.unit_menu)

  -- Loads tactics menu
  self.available_tactics = tactic_params
  self.tactic_menu = NumMenu(tactic_params)
  self.tactic_menu.position = Vec(600, h/2-200)
  self:view('hud'):add('tactic_menu', self.tactic_menu)
end

-- Creates castle and initializes waves
function PlayStageState:_load_units()
  local pos = self.battlefield:tile_to_screen(-6, 6)
  self:_create_capital_at(pos)
  local wave_copy = Util.deepcopy(self.stage.waves[1])
  self.wave = Wave(wave_copy)
  self.wave:start(10)
  self.support_range = Range(self.battlefield)
  self:view('bg'):add('range', self.support_range)
end

-- Functions for dealing with unit requirements
function PlayStageState:_get_available_units()
  local unit_files = love.filesystem.getDirectoryItems('database/entities/units')

  local all_units = {}
  for i,v in ipairs(unit_files) do
    all_units[i] = Util.split(v, '.')[1]
  end

  for _, possible_unit_name in ipairs(all_units) do
    -- Check if the unit has requirements
    local possible_unit = Unit(possible_unit_name)
    local requirements = possible_unit:get_requirement()
    local max = possible_unit:get_max_per_unit()

    if not possible_unit:evolves_from() then

      if requirements then
        table.insert(self.unavailable_units, {
          name = possible_unit_name,
          pretty_name = possible_unit:get_name(),
          left = max and 0 or nil,
          position = possible_unit:get_limited_position(),
          requires = requirements
        })
      else
        local left = max or nil
        table.insert(self.available_units, {
          name = possible_unit_name,
          pretty_name = possible_unit:get_name(),
          left = left,
          position = nil
        })
      end
    end
  end
end

function PlayStageState:_update_available_units(specname)
  -- Check if any unavailable unit will become available
  for i, unit in ipairs(self.unavailable_units) do
    for j, file_name in ipairs(unit.requires) do
      if file_name == specname then
        -- If unit has only 1 requirement left, move it from unavailable list to available list
        if #unit.requires == 1 then
          table.insert(self.available_units,
            { name = unit.name, pretty_name = unit.pretty_name,
              left = unit.left, position = unit.position })
          table.remove(self.unavailable_units, i)
        -- Else, remove requirement
        else table.remove(self.unavailable_units[i].requires, j) end
        break
      end
    end
  end

  -- Update units left in available units
  for _, available_unit in ipairs(self.available_units) do
    local unit = Unit(available_unit.name)

    -- if the unit is already used
    if unit:get_name() == specname and available_unit.left
      and available_unit.left > 0 then
      available_unit.left = available_unit.left - 1
      if available_unit.left == 0 then
          self.available_positions:_reset_positions()
      end

    -- if the unit is a requirement
    elseif unit:get_requirement() then
      for _, requirement in ipairs(unit:get_requirement()) do
        if requirement == specname and unit:get_max_per_unit() then
          available_unit.left = available_unit.left + unit:get_max_per_unit()
        end
      end
    end
  end
end

function PlayStageState:_add_bonus(fn, bonus)
  -- Increases the limit of available units of each type by <bonus>
  for i, unit in ipairs(self.available_units) do
    if unit.left and fn(Unit(unit.name)) then
      self.available_units[i].left = unit.left + bonus
    end
  end
  for i, unit in ipairs(self.unavailable_units) do
    if unit.left and fn(Unit(unit.name)) then
      self.unavailable_units[i].left = unit.left + bonus
    end
  end
end

-- Functions for creating and evolving units and enemies
function PlayStageState:_create_capital_at(pos)
  local unit = Entity(nil, 'capital')
  self.atlas:add(unit, pos, unit:get_appearance())
  table.insert(self.units, {unit, pos})
  self.unit_pos.capital = {}
  table.insert(self.unit_pos.capital, pos)
  self.capital = unit
end

function PlayStageState:_create_unit_at(specname, pos)
  local tile = self.battlefield:round_to_tile(pos)
  -- If trying to create unit over an existing one, tries to evolve it
  for i, crew in ipairs(self.units) do
    local position = crew[2]
    local existing_unit = crew[1]
    if self.battlefield:round_to_tile(position) == tile then
      if existing_unit:get_evolution() then
        self:_evolve_unit_at(i, existing_unit:get_evolution(), position)
      else
        return
      end
    end
  end
  local unit = Unit(specname)
  if self.gild >= unit:get_cost() then
    if unit:get_damage() > 0 then
      self.audio:playSound('hire')
    else
      self.audio:playSound('build')
    end
    self.atlas:add(unit, pos, unit:get_appearance())
    table.insert(self.units, {unit, pos})
    if self.unit_pos[specname] == nil then
      self.unit_pos[specname] = {}
    end
    table.insert(self.unit_pos[specname], pos)
    self.gild = self.gild - unit:get_cost()

    -- Update available units
    self:_update_available_units(unit:get_name())

    -- Add bonus
    local bonus = unit:get_bonus()
    if bonus then
      if bonus.units == 'warriors' then
        local function fn(u) return u:get_damage() > 0 end
        self:_add_bonus(fn, bonus.n)
      end
    end

    -- Update menu
    self.unit_menu:_update(self.available_units, specname)

    -- Add support
    local support = unit:get_support()
    if support then
      table.insert(self.support_units, { name = unit:get_name(), pos = pos, support = support })
      self.support_range:_add_unit(unit, pos, support.range)
      if support.entity == 'unit' then self:_add_support(pos, support) end
    end
    self:_check_support()

    return unit
  end
end

function PlayStageState:manage_spawn()
  local rand = love.math.random
  while self.wave:poll() > 0 do
    local x, y = rand(5, 7), -rand(5, 7)
    local type = self.wave:spawn()
    if type ~= 'end' then
      local pos = self.battlefield:tile_to_screen(x, y)
      local monster = self:_create_enemy_at(type, pos)
      self.monsters[monster] = true
    end
  end
end

function PlayStageState:_create_enemy_at(specname, pos)
  local unit = Enemy(specname)
  if string.match(unit:get_name(), 'Slime') then self.audio:playSound('slime') end
  if string.match(unit:get_name(), 'Berserker') then self.audio:playSound('berserker') end
  self.atlas:add(unit, pos, unit:get_appearance())
  table.insert(self.monsters, {unit, pos})
  return unit
end

function PlayStageState:_evolve_unit_at(index, specname, pos)
  local old_unit = self.units[index][1]
  local new_unit = Unit(specname)
  -- Evolves a unit by killing it and replacing with a better one
  if self.gild >= new_unit:get_cost() then
    self.atlas:add(new_unit, pos, new_unit:get_appearance())
    table.insert(self.units, {new_unit, pos})
    if self.unit_pos[specname] == nil then
      self.unit_pos[specname] = {}
    end
    table.insert(self.unit_pos[specname], pos)
    self.gild = self.gild - new_unit:get_cost()
    old_unit:take_damage(999999999999)
  end
end

function PlayStageState:_find_all_possible_positions(name, distance)
  for _,unit in ipairs(self.units) do
    if unit[1]:get_name() == name then
      self.available_positions:_set_positions(unit[2], distance)
    end
  end
end

-- Functions for dealing with support units
function PlayStageState:_add_support(pos, support)
  for _,unit in ipairs(self.units) do
    local unit_unit = unit[1]
    local unit_pos = unit[2]
    -- Multiplies a stat from a unit by <effect>
    if unit_pos ~= pos and Util.is_in_range(unit_pos, pos, support.range) then
      unit_unit:update_component(support.component, support.effect)
    end
  end
end

function PlayStageState:_check_support()
  local unit_unit = self.units[#self.units][1]
  local unit_pos = self.units[#self.units][2]

  for _,support_unit in ipairs(self.support_units) do
    local support = support_unit.support
    local support_pos = support_unit.pos

    if support.entity == 'unit' and unit_pos ~= support_unit.pos and
      Util.is_in_range(unit_pos, support_pos, support.range) then
      unit_unit:update_component(support.component, support.effect)
    end
  end
end

function PlayStageState:_remove_support_unit(name, pos)
  for i,unit in ipairs(self.support_units) do
    if unit.name == name and unit.pos == pos then
      table.remove(self.support_units, i)
      return i
    end
  end
end

-- Functions for dealing with player input
function PlayStageState:on_mousepressed(_, _, button)
  -- Left mouse button creates units or evolves existing ones
  if button == 1 then
    local option = self.unit_menu:current_option()
    local positions = self.available_positions:get_positions()
    if #positions == 0 or Util._is_a_valid_position(positions, self.cursor) then
      self:_create_unit_at(self.available_units[option].name, Vec(self.cursor:get_position()))
    end
  -- Right mouse button navigates the menu
  else
    self.unit_menu:next()
    self.available_positions:_reset_positions()
    local option = self.unit_menu:current_option()
    local unit = Unit(self.available_units[option].name)
    local position = unit:get_limited_position()
    self.info:set_text(unit:get_stats())

    if position then
      self:_find_all_possible_positions(position.name, position.distance)
    end
  end
end

-- Functions for dealing with tactics
function PlayStageState:on_keypressed(key)
  -- Deselects current tactic
  if key == 'escape' then
    self.current_tactic = nil
    self.tactics_positions:_reset_positions()
  end
  local num = tonumber(key)
  -- Selects or execute tactic
  if num ~= nil and self.tactics[num] ~= nil then
    if self.current_tactic == num then
      self:execute_tactic(num)
    else
      self:select_tactic(num)
    end
  end
end

function PlayStageState:select_tactic(index)
  self.current_tactic = index
  local tactic = self.tactics[index]
  local cursor_pos = Vec(self.cursor:get_position())
  self.tactics_positions:_set_positions(cursor_pos, tactic:get_range())
  self.tactics_positions:set_color(tactic:get_color())
end

function PlayStageState:execute_tactic(index)
  local tactic = self.tactics[index]
  local cost = tactic:get_cost()
  local effect = tactic:get_effect()
  if self.gild >= cost then
    self:announcement(tactic:get_message(true), 2)
    self:update_gild(-cost)
    local list
    if effect[1] == 'unit' then
      list = self.units
    elseif effect[1] == 'monster' then
      list = self.monsters
    else
      list = Util.concat(self.units, self.monsters)
    end
    for _, table in ipairs(list) do
      local entity = table[1]
      local pos = table[2]
      if self.tactics_positions:is_in_range(pos) then
        entity:take_damage(effect[2])
      end
    end
  else
    self:announcement(tactic:get_message(false), 2)
  end
end

function PlayStageState:announcement(message, time)
  self.log:set_text(message)
  self.announce = time
end

-- Functions for dealing with units taking/receiving damage
function PlayStageState:check_collisions(dt)
  for _, monster in ipairs(self.monsters) do
    local monster_unit = monster[1]
    local sprite_instance = self.atlas:get(monster_unit)
    local pos = sprite_instance.position
    local target_list = monster_unit:get_target()
    local target = nil
    local moving = monster_unit:is_moving()

    -- Sets a target for the monster
    for _, t in ipairs(target_list) do
      if self.unit_pos[t] then
        target = self.unit_pos[t][1]
        break
      end
    end

    target = target or self.unit_pos['capital'][1]

    local speed = monster_unit:get_speed()

    -- check if enemy if affected by a support unit
    for _,unit in ipairs(self.support_units) do
      local in_range = Util.is_in_range(sprite_instance.position, unit.pos, unit.support.range)
      if unit.support.component == 'speed' and in_range then
        speed = speed*unit.support.effect
      end
    end

    local target_pos = target - pos

    local new_pos = target_pos:normalized() * speed * dt
    if moving then sprite_instance.position:add(new_pos) end

    local radius = monster_unit:get_range()

    for _, table in ipairs(self.units) do
      local unit = table[1]
      local pos_unit = table[2]
      local unit_radius = unit:get_range()
      if unit:is_a() ~= Enemy then
        local dist = (pos_unit - pos)
        dist = dist:length()
        if dist < radius then
          if unit:get_name() == 'Capital' then monster[1]:halt() end
          unit:take_damage(monster_unit:get_damage())
        end
        if dist < unit_radius then
          monster_unit:take_damage(unit:get_damage())
        end
      end
    end
  end
end

function PlayStageState:check_deaths()
  local end_wave = true
  -- Checks if any enemy has died on this frame
  for i, monster in ipairs(self.monsters) do
    end_wave = false
    local monster_unit = monster[1]
    if not monster_unit:is_alive() then
      table.remove(self.monsters, i)
      self.atlas:remove(monster_unit)
    end
  end

  -- Checks if the castle was destroyed on this frame
  if end_wave and self.wave:has_ended() then
    self.current_wave = self.current_wave + 1
    if self.current_wave >= #self.stage.waves then
      return self:push('victory_stage', {victory = true})
    end
    local wave_copy = Util.deepcopy(self.stage.waves[self.current_wave])
    self.wave = Wave(wave_copy)
    self.wave:start(10)
  end

  -- Checks if any player unit has died on this frame
  for i, unit in ipairs(self.units) do
    local unit_unit = unit[1]
    local unit_pos = unit[2]
    if not unit_unit:is_alive() then
      table.remove(self.units, i)
      self.atlas:remove(unit_unit)
      self.support_range:_remove_unit(self:_remove_support_unit(unit_unit:get_name()), unit_pos)
      if unit_unit:get_name() == 'Capital' then
        self.game_over = true
        break
      end
      local possibilities = self.unit_pos[unit_unit:get_db_name()]
      for j, pos in ipairs(possibilities) do
        if pos == unit_pos then
          table.remove(possibilities, j)
          if #self.unit_pos[unit_unit:get_db_name()] == 0 then
            self.unit_pos[unit_unit:get_db_name()] = nil
          end
        end
      end
    end
  end
end

-- Functions for dealing with units that produces gold
function PlayStageState:produce_value(dt)
  for _, table in ipairs(self.units) do
    local unit = table[1]
    if unit:get_value() > 0 then
      self.gild = self.gild + unit:get_value()*dt
    end
  end
end

function PlayStageState:update_gild(value)
  self.gild = self.gild + value
end

-- Updates everything
function PlayStageState:update(dt)
  -- Checks for a game over
  if self.game_over and self.delay <= 0 then
    return self:push('victory_stage', {victory = false})
  elseif self.game_over then
    self.log:set_text("The castle has been destroyed!")
    self.delay = self.delay - dt
  end

  -- Updates gold
  self:produce_value(dt)
  self.stats:_update(self.gild)

  -- Updates range for selected tactic (if such tactic exists)
  if self.current_tactic then
    self.tactics_positions:_update(Vec(self.cursor:get_position()))
  end

  -- Sets the message on the log
  if not self.wave:has_started() and not self.wave:has_ended() then
    local delay = math.floor(self.wave:update(dt))
    self.log:set_text(delay .. " seconds until next wave")
  elseif self.announce > 0 then
    self.announce = self.announce - dt
  elseif not self.game_over then
    self.wave:update(dt)
    self.log:set_text("Wave in progress...")
  end

  self:manage_spawn()
  self:check_collisions(dt)
  self:check_deaths()
end

return PlayStageState
