
return {
  name = "Archer Troop",
  max_hp = 150,
  appearance = 'archer',
  damage = 2,
  range = 60,
  cost = 200,
  value = 0,
  requires = { 'Tower Building' },
  max_per_unit = 4,
  limited_position = { name = 'Tower Building', distance = 1 },
  evolution = "elite_archer"
}

