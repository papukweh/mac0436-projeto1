
return {
  name = "Farm Building",
  max_hp = 500,
  appearance = 'farm',
  damage = 0,
  range = 0,
  cost = 500,
  value = 10,
  bonus = { n = 1, units = 'warriors' },
  evolution = "bigger_farm"
}

