
return {
  name = "Elite Warrior Troop",
  max_hp = 500,
  appearance = 'elite_knight',
  damage = 4,
  range = 40,
  cost = 500,
  value = 0,
  evolves_from = "warrior"
}