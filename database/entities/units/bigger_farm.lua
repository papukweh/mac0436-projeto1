
return {
  name = "Bigger Farm Building",
  max_hp = 1000,
  appearance = 'big_farm',
  damage = 0,
  range = 0,
  cost = 750,
  value = 50,
  evolves_from = "farm"
}

