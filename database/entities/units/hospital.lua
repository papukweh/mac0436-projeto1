
return {
  name = "Hospital Building",
  max_hp = 300,
  appearance = 'hospital',
  damage = 0,
  range = 0,
  cost = 500,
  value = 0,
  support = { entity = 'unit', component = 'hp', effect = 1.1, range = 50 }
}

