
return {
  name = "Warrior Troop",
  max_hp = 150,
  appearance = 'knight',
  damage = 5,
  range = 35,
  cost = 100,
  value = 0,
  evolution = "elite_warrior"
}