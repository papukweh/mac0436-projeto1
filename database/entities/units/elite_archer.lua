
return {
  name = "Elite Archer Troop",
  max_hp = 250,
  appearance = 'elite_archer',
  damage = 2,
  range = 75,
  cost = 500,
  value = 0,
  evolves_from = "archer"
}