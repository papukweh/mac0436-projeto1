
return {
  name = "Sage Troop",
  max_hp = 150,
  appearance = 'sage',
  damage = 4,
  range = 50,
  cost = 300,
  value = 0,
  requires = { 'School Building' },
  max_per_unit = 2,
  limited_position = { name = 'School Building', distance = 2 }
}

