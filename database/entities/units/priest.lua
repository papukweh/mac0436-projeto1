
return {
  name = "Priest Troop",
  max_hp = 250,
  appearance = 'priest',
  damage = 2,
  range = 50,
  cost = 100,
  value = 0,
  requires = { 'Hospital Building' },
  max_per_unit = 2,
  limited_position = { name = 'Hospital Building', distance = 1 }
}

