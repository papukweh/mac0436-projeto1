
return {
  name = "Spider Web Support",
  max_hp = 500,
  appearance = 'web',
  damage = 0,
  range = 20,
  cost = 50,
  value = 0,
  support = { entity = 'enemy', component = 'speed', effect = .7, range = 70 }
}
