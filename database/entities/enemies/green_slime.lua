
return {
  name = "Green Slime",
  max_hp = 150,
  appearance = 'slime',
  range = 40,
  speed = 200,
  damage = 1,
  target = {'capital'}
}