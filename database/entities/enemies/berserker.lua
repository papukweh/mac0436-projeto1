
return {
  name = "Berserker",
  max_hp = 750,
  appearance = 'berserker',
  range = 35,
  speed = 10,
  damage = 7,
  target = {'warrior', 'archer', 'sage', 'elite_warrior', 'elite_archer'}
}