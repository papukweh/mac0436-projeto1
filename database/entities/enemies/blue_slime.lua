
return {
  name = "Blue Slime",
  max_hp = 250,
  appearance = 'blue_slime',
  range = 50,
  speed = 100,
  damage = 2,
  target = {'farm', 'tower', 'capital'}
}