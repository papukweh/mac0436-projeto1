
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },
    citadel = {
      frame = { 5, 19 },
      color = 'white'
    },
    cursor = {
      frame = { 29, 14 },
      color = 'gray'
    },
    slime = {
      frame = { 27, 8 },
      color = 'green'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue'
    },
    berserker = {
      frame = { 28, 2 },
      color = 'red'
    },
    knight = {
      frame = { 28, 0 },
      color = 'blue'
    },
    elite_knight = {
      frame = { 28, 0 },
      color = 'yellow'
    },
    archer = {
      frame = { 32, 0 },
      color = 'red'
    },
    elite_archer = {
      frame = { 32, 0 },
      color = 'yellow'
    },
    priest = {
      frame = { 24, 0 },
      color = 'white'
    },
    farm = {
      frame = { 0, 21 },
      color = 'white'
    },
    tower = {
      frame = { 2, 19 },
      color = 'gray'
    },
    sage = {
      frame = { 24, 1 },
      color = 'purple'
    },
    hospital = {
      frame = { 2, 12 },
      color = 'yellow'
    },
    school = {
      frame = { 5, 20 },
      color = 'blue'
    },
    web = {
      frame = { 2, 15 },
      color = 'white'
    },
    big_farm = {
      frame = { 0, 21 },
      color = 'yellow'
    }
  }
}
