return {
    name = "Meteor",
    cost = 1000,
    effect = {'all', 5},
    range = 4,
    color = "red",
    message_success = "The battlefield turns into a blazing inferno",
    message_failure = "You can't afford a single spark!"
}