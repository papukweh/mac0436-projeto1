return {
    name = "Rain of Arrows",
    cost = 500,
    effect = {'monster', 5},
    range = 3,
    color = "yellow",
    message_success = "Your archers cover the skies with arrows!",
    message_failure = "Do you think arrows grow on trees?"
}