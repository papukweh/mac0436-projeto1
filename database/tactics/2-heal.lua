return {
    name = "Song of Healing",
    cost = 700,
    effect = {'unit', -10},
    range = 2,
    color = "blue",
    message_success = "Your army hears music and feels... better?",
    message_failure = "Musicians need to be paid, you know..."
}