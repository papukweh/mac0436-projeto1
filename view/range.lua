
local ATLAS_DB    = require 'database.atlas'
local PALETTE_DB = require 'database.palette'
local Range = require 'common.class' ()

function Range:_init(battlefield)
  self.borders = battlefield.bounds
  self.units = {}
end

local function get_color(name)
  local color_name = ATLAS_DB.sprites[name].color
  return PALETTE_DB[color_name]
end

function Range:_add_unit(unit, center, range)
  local x, y = center:get()
  table.insert(self.units, {
    name = unit:get_name(),
    color = get_color(unit:get_appearance()),
    x = x - range < self.borders.left and self.borders.left or x - range,
    y = y - range < self.borders.top and self.borders.top or y - range,
    width = x + range > self.borders.right and self.borders.right - x + range or 2*range,
    height = y + range > self.borders.bottom and self.borders.bottom - y + range or 2*range
  })
end

function Range:_remove_unit(index)
  table.remove(self.units, index)
end

function Range:draw()
  local g = love.graphics
  g.push()
  g.setLineWidth(1)
  for _,unit in ipairs(self.units) do
    g.setColor(unit.color[1], unit.color[2], unit.color[3], .2)
    g.rectangle('fill', unit.x, unit.y, unit.width, unit.height)
  end
  g.pop()
end

return Range
