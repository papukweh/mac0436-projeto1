
local Stats = require 'common.class' ()

function Stats:_init(position, initial_gild)
  self.position = position
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
  self.gild = initial_gild
end

function Stats:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.print(("Gild %d"):format(self.gild))
  g.translate(0, self.font:getHeight())
  g.pop()
end

function Stats:_update(gild)
  self.gild = gild
end

return Stats

