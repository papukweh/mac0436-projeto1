
local Vec = require 'common.vec'

local ListMenu = require 'common.class' ()

ListMenu.GAP = 4
ListMenu.PADDING = Vec(24, 8)

local function get_text(option)
  -- if option is a table
  if type(option) == 'table' then
    if not option.left then return option.pretty_name end
    return option.pretty_name .. ' (' .. option.left .. ')'
  end

  -- else, is a string
  return option
end

function ListMenu:_set_options(options, current)
  local current_index = 0

  self.options = {}
  local vert_offset = 0
  local max_width = 0

  for i, option in ipairs(options) do
    if type(option) ~= 'table' or not option.left or option.left > 0 then
      self.options[i] = love.graphics.newText(self.font, get_text(option))
      local width, height = self.options[i]:getDimensions()
      if width > max_width then
        max_width = width
      end
      vert_offset = vert_offset + height + ListMenu.GAP

      if current and current == option.name then current_index = i end
    else
      self.options[i] = love.graphics.newText(self.font, '')
    end
  end

  self.size = Vec(max_width, vert_offset)

  return current_index > 0 and current_index or 1
end

function ListMenu:_init(options)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec()
  self:_set_options(options)
  self.current = 1
end

function ListMenu:reset_cursor()
  self.current = 1
end


function ListMenu:next()
  if self.current == #self.options then
    self.current = 1
  else
    local index = self.current
    repeat index = index + 1
    until index > #self.options or self.options[index]:getWidth() > 0
    self.current = index > #self.options and 1 or index
  end
end


function ListMenu:previous()
  if self.current == 1 then
    self.current = #self.options
  else
    self.current = math.max(1, self.current - 1)
  end
end

function ListMenu:current_option()
  return self.current
end

function ListMenu:get_dimensions()
  return (self.size + ListMenu.PADDING * 2):get()
end

function ListMenu:draw()
  local g = love.graphics
  local size = self.size + ListMenu.PADDING * 2
  local voffset = 0
  local x, y = self.position:get()
  local _, sizeH = size:get()
  g.push()
  g.translate(x, y - sizeH/2)
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, size:get())
  g.translate(ListMenu.PADDING:get())
  for i, option in ipairs(self.options) do
    local height = option:getHeight()
    if i == self.current then
      local left = - ListMenu.PADDING.x * 0.5
      local right = - ListMenu.PADDING.x * 0.25
      local top, bottom = voffset + height * .25, voffset + height * .75
      g.polygon('fill', left, top, right, (top + bottom) / 2, left, bottom)
    end
    g.setColor(1, 1, 1)
    g.draw(option, 0, voffset)
    voffset = voffset + height + ListMenu.GAP
  end
  g.pop()
end

function ListMenu:_update(newOptions, current)
  -- Reset self.options
  self.options = {}
  self.current = self:_set_options(newOptions, current)
end

return ListMenu

