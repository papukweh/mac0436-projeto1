local a = love.audio

local Audio = require 'common.class' ()

function Audio:_init()
    self.songs = {}
    self.sounds = {}
    self:load_music()
    self:load_sfx()
    self.playing = false
end

function Audio:load_music()
    self.songs['menu_music'] = Audio:load_loop('01 - Opening.ogg')
    self.songs['battle_music'] = Audio:load_loop('13 - Danger.ogg')
    self.songs['victory_music'] = Audio:load_loop('17 - Victory.ogg')
    self.songs['game_over'] = Audio:load_loop('20 - Game Over.ogg')
end

function Audio:load_loop(path)
    local music = a.newSource('assets/sources/' .. path, 'stream')
    music:setLooping(true)
    self.playing = true
    return music
end

function Audio:load_sfx()
    self.sounds['slime'] = a.newSource('assets/sources/slime8.wav', 'static')
    self.sounds['berserker'] = a.newSource('assets/sources/giant2.wav', 'static')
    self.sounds['attack'] = a.newSource('assets/sources/sword-unsheathe2.wav', 'static')
    self.sounds['hire'] = a.newSource('assets/sources/coin3.wav', 'static')
    self.sounds['build'] = a.newSource('assets/sources/metal-small2.wav', 'static')
end

function Audio:stop()
    self.playing = false
    a.stop()
end

function Audio:playMusic(music)
    a.play(self.songs[music])
end

function Audio:playSound(sfx)
    a.play(self.sounds[sfx])
end

return Audio
