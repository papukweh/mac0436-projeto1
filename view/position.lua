
local PALETTE_DB = require 'database.palette'
local Vec = require 'common.vec'
local Position = require 'common.class' ()

local CELL_SIZE = 32

function Position:_init(battlefield, color)
  self.battlefield = battlefield
  self.positions = {}
  self.color = PALETTE_DB[color]
  self.distance = 0
end

function Position:set_color(color)
  self.color = PALETTE_DB[color]
end

function Position:is_in_range(position)
  local tile = self.battlefield:round_to_tile(position)
  for _, pos in ipairs(self.positions) do
    if tile == pos then
      return true
    end
  end
  return false
end

function Position:_set_positions(center, distance)
  self.distance = distance
  local bounds = self.battlefield.bounds
  local minX, maxX = bounds.left, bounds.right
  local minY, maxY = bounds.top, bounds.bottom

  for dist=1, distance do
    for i=-dist, dist do
      for j=-dist, dist do
        local possible_pos = Vec(i, j)*CELL_SIZE + center
        local x, y = possible_pos:get()

        if x > minX and x < maxX and y > minY and y < maxY then
          table.insert(self.positions, self.battlefield:round_to_tile(possible_pos))
        end
      end
    end
  end
end

function Position:_update(center)
  self:_reset_positions()
  self:_set_positions(center, self.distance)
end

function Position:get_positions()
  return self.positions
end

function Position:_reset_positions()
  self.positions = {}
end

function Position:draw()
  local g = love.graphics
  g.push()
  g.setColor(self.color)
  g.setLineWidth(1)
  g.translate(-CELL_SIZE/2, -CELL_SIZE/2)
  for _,pos in ipairs(self.positions) do
    local x, y = pos:get()
    g.rectangle('line', x, y, CELL_SIZE, CELL_SIZE)
  end
  g.pop()
end

return Position

