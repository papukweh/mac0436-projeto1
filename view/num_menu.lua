
local Vec = require 'common.vec'

local NumMenu = require 'common.class' ()

NumMenu.GAP = 4
NumMenu.PADDING = Vec(24, 8)

function NumMenu:_init(options)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 24)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec()
  self.options = {}
  local vert_offset = 0
  local max_width = 0
  for i, option in ipairs(options) do
    self.options[i] = love.graphics.newText(self.font, option)
    local width, height = self.options[i]:getDimensions()
    if width > max_width then
      max_width = width
    end
    vert_offset = vert_offset + height + NumMenu.GAP
  end
  self.size = Vec(max_width, vert_offset)
  self.current = 1
end

function NumMenu:get_dimensions()
  return (self.size + NumMenu.PADDING * 2):get()
end

function NumMenu:draw()
  local g = love.graphics
  local size = self.size + NumMenu.PADDING * 2
  local voffset = 0
  g.push()
  g.translate(self.position:get())
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, size:get())
  g.translate(NumMenu.PADDING:get())
  for _, option in ipairs(self.options) do
    local height = option:getHeight()
    g.setColor(1, 1, 1)
    g.draw(option, 0, voffset)
    voffset = voffset + height + NumMenu.GAP
  end
  g.pop()
end

return NumMenu
