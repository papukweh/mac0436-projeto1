
local Vec = require 'common.vec'

local Text = require 'common.class' ()

function Text:_init(text, x, y, size_x, size_y, color, font_size)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', font_size)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec(x,y)
  self.color = color
  self.text = love.graphics.newText(self.font, text)
  self.size = Vec(size_x, size_y)
end

function Text:get_dimensions()
  return (self.size + Text.PADDING * 2):get()
end

function Text:set_text(newText)
  self.text = love.graphics.newText(self.font, newText)
end

function Text:draw()
  local g = love.graphics
  g.push()
  g.translate(self.position:get())
  g.setColor(self.color)
  g.draw(self.text, 0,0)
  g.pop()
end

return Text

